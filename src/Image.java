import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;

// This class represents a simple rectangular image, where each pixel can be
// one of 16 colours.
public class Image {

    // Store a 2 dimensional image with "colours" as numbers between 0 and 15
    private int pixels[][];
    // Set the value inside pixels.
    public void setPixelsValue(int height, int width, int value) { pixels[height][width] = value; }

    // Read in an image from a file. Each line of the file must be the same
    // length, and only contain single digit hex numbers 0-9 and a-f.
    public Image(String filename) {

        // Read the whole file into lines
        ArrayList<String> lines = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            for (String s = in.readLine(); s != null; s = in.readLine())
                lines.add(s);
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found: " + filename);
            System.exit(1);
        }
        catch (IOException e) {
            System.exit(2);
        }

        if (lines.size() == 0) {
            System.out.println("Empty file: " + filename);
            System.exit(1);
        }

        // Initialise the array based on the number of lines and the length of the
        // first one.
        int length = lines.get(0).length();
        pixels = new int[lines.size()][length];

        for (int i = 0; i < lines.size(); i++) {
            // Check that all of the lines have the same length as the first one.
            if (length != lines.get(i).length()) {
                System.out.println("Inconsistent line lengths: " + length + " and " + lines.get(i).length() + " on lines 1 and " + (i+1));
                System.exit(1);
            }

            // Copy each line into the array
            for (int j = 0; j < length; j++) {
                pixels[i][j] = Character.getNumericValue(lines.get(i).charAt(j));
                if (pixels[i][j] < 0 || pixels[i][j] > 15) {
                    System.out.println("Invalid contents: " + lines.get(i).charAt(j) + " on line " + (i+1));
                    System.exit(1);
                }
            }
        }
    }

    // Create a solid image with given dimensions and colour
    public Image(int height, int width, int colour) {
        pixels = new int[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                pixels[i][j] = colour;
    }

    // Get back the original text-based representation
    public String toString() {
        StringBuilder s = new StringBuilder(pixels.length * pixels[0].length);
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++)
                s.append(Integer.toHexString(pixels[i][j]));
            s.append("\n");
        }
        return s.toString();
    }

    // TASK 2: Implement the compress method to create and return a list of
    // drawing commands that will draw this image.
    // 6 marks for correctness -- does the command list exactly produce the
    // input image.
    // 5 marks for being able to shrink test-image1 and test-image2 into no more
    // than 7 commands each. You can work out these commands by hand, but the
    // marks here are for your implemented algorithm (HINT: think Run-length
    // Encoding) being able to create the commands.
    // 4 marks for shrinking the other, more difficult, test images. We'll run
    // this as a competition and give all 4 to the best 20% of the class, 3 to
    // the next best 20%, and so on.
    public Drawing compress() {
        //                                Height,         Length,       Colour
        Drawing result = new Drawing( pixels.length, pixels[0].length, 0);

        //Algorithm created list of commands
        ArrayList<DrawingCommand> Original = orginalCompression();
        ArrayList<DrawingCommand> Algorithm1 = left2rightCompression();
        ArrayList<DrawingCommand> Algorithm2 = top2bottomCompression();
        //ArrayList<DrawingCommand> Algorithm3 = diaganalCompression();
        //Algorithm created list of commands END

        //Debugging Purposes
        System.out.println("Original: " + Original.size());
        System.out.println("Algor1: " + Algorithm1.size());
        System.out.println("Algor2: " + Algorithm2.size());
        //System.out.println("Algor3: " + Algorithm3.size());
        //Debugging Purposes END

        //Algorithm Testing and Validation
        ArrayList<DrawingCommand> commandList = Original;

        if(commandList.size() > Algorithm1.size() && ValidateAlgorithm(Algorithm1, pixels)) {
            commandList = Algorithm1;
            System.out.println("Algorithm 1");
        }
        if (commandList.size() > Algorithm2.size() && ValidateAlgorithm(Algorithm2, pixels)) {
            commandList = Algorithm2;
            System.out.println("Algorithm 2");
        }
        //if (commandList.size() > Algorithm3.size() && ValidateAlgorithm(Algorithm3, pixels)) {
        //    commandList = Algorithm3;
        //    System.out.println("Algorithm 3");
        //}
        if (commandList == Original) {
            System.out.println("Using Original Algorithm");
        }

        for(DrawingCommand commands : commandList)
            result.addCommand(commands);
        //Algorithm Testing and Validation END

        return result;
    }

    // This is the standard 4-bit EGA colour scheme, where the numbers represent
    // 24-bit RGB colours.
    static int[] colours =
            {0x000000, 0x0000AA, 0x00AA00, 0x00AAAA,
                    0xAA0000, 0xAA00AA, 0xAA5500, 0xAAAAAA,
                    0x555555, 0x5555FF, 0x55FF55, 0x55FFFF,
                    0xFF5555, 0xFF55FF, 0xFFFF55, 0xFFFFFF};

    // Render the image into a PNG with the given filename.
    public void toPNG(String filename) {

        BufferedImage im = new BufferedImage(pixels[0].length, pixels.length, BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < pixels.length; i++)
            for (int j = 0; j < pixels[i].length; j++) {
                im.setRGB(j,i,colours[pixels[i][j]]);
            }

        File f = new File(filename + ".png");
        try {
            ImageIO.write(im,"PNG",f);
        }
        catch (IOException e) {
            System.out.println("Unable to write image");
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        // A simple test to read in an image and print it out.
        String name = "1";
        for(int g = 1; g <7; g++) {
            Image i = new Image("/Users/nikkel/Documents/ACE_ASS/Ass1/src/test-images/pixel-art" + g);
            System.out.println(i.toString());
            Drawing draw = i.compress();
            System.out.println(draw.commands.size() + " Object string below");
            //System.out.println(draw.toString())

            FileWriter writer = null;
            try {
                writer = new FileWriter("pixel-art-drawing" + g);
                writer.write(draw.toString());
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Image drawedImage = draw.draw();
                System.out.println(drawedImage.toString());
            } catch (BadCommand badCommand) {
                badCommand.printStackTrace();
            }
            System.out.println(draw.toString());
            //i.toPNG("/Users/nikkel/Documents/ACE_ASS/Ass1/src/test-images/test-image1");
        }
    }

    //Quite low but extremely reliable and will work for any image.
    public ArrayList<DrawingCommand> orginalCompression()
    {
        //Field
        ArrayList<DrawingCommand> DrawingCommands = new ArrayList<DrawingCommand>();
        Direction direction = Direction.RIGHT;

        for(int i = 0; i < pixels.length ; i++)//height check
        {
            //Field (In loop)
            int j = 0;
            //Field END

            //Check First
            if(i == 0) {
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
                j++; //increment j
            } else {
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 1 + " " + Integer.toHexString(pixels[i][j])));
                j++;
            }
            //Check First END

            //First Loop Increment
            while(j < pixels[0].length)
            {
                int data = pixels[i][j];
                int runLength = 0;
                while(j < pixels[i].length && pixels[i][j] == data)
                {
                    j++;
                    runLength++;
                }
                DrawingCommands.add(new DrawingCommand(direction+" "+((runLength == 1) ? 1 : runLength)+" "+Integer.toHexString(data)));
            }
            //First Loop Increment END

            //Second Check (Down)
            i++; j--; //Make J 19
            if(i > pixels.length) {
                continue;
            }
            else {
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 1 + " " + Integer.toHexString(pixels[i][j])));
            }
            j--;
            //Second Check (Down) END

            //First Direction Change
            direction = (direction == Direction.LEFT) ? Direction.RIGHT : Direction.LEFT;
            //First Direction Change END

            //Second Loop Increment
            while(j >= 0) // Deincrement
            {
                int data = pixels[i][j];
                int runLength = 0;
                while(j >= 0 && pixels[i][j] == data)
                {
                    j--;
                    runLength++;
                }
                DrawingCommands.add(new DrawingCommand(direction+" "+((runLength == 1) ? 1 : runLength)+" "+Integer.toHexString(data)));
            }
            //Second Loop Increment END

            //Second Direction Change
            direction = (direction == Direction.LEFT) ? Direction.RIGHT : Direction.LEFT;
            //Second Direction Change End
        }
        return DrawingCommands;
    }

    //Not every good but it works can be broken
    public ArrayList<DrawingCommand> left2rightCompression()
    {
        ArrayList<DrawingCommand> DrawingCommands = new ArrayList<DrawingCommand>();

        //Left to Right runLength compression
        Direction direction = Direction.RIGHT;
        for(int i = 0; i < pixels.length ; i += 2)//height check
        {
            //Check if the background is the same as the whole first row and not the second.
            if(i == 0 && row1and2Check(pixels[0], pixels[1])) {
                i++;
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 1 + ""));
                DrawingCommands.add(new DrawingCommand((Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][pixels[0].length - 1]))));
            } else if(i == 0) //Command to paint first block
                DrawingCommands.add(new DrawingCommand(Direction.DOWN+" "+0+" "+Integer.toHexString(pixels[0][0])));
            else {
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 2 + ""));/* + " " + Integer.toHexString(pixels[i][(pixels[0].length) - 1])));*/
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][pixels[0].length-1])));
            }
            int j = 0;
            while(j < pixels[0].length)
            {
                int data = pixels[i][j];
                int runLength = 0;
                while(j < pixels[i].length && pixels[i][j] == data)
                {
                    j++;
                    runLength++;
                }
                DrawingCommands.add(new DrawingCommand(direction+" "+(runLength-1)+" "+Integer.toHexString(data)));
                //direction = (direction == Direction.LEFT) ? Direction.RIGHT : Direction.LEFT;
            }
            direction = (direction == Direction.LEFT) ? Direction.RIGHT : Direction.LEFT;
        }

        return DrawingCommands;
    }

    //Okay but can be quite linear and can be broken
    public ArrayList<DrawingCommand> top2bottomCompression()
    {
        ArrayList<DrawingCommand> DrawingCommands = new ArrayList<DrawingCommand>();

        //Left to Right runLength compression
        Direction direction = Direction.DOWN;
        for(int i = 0; i < pixels[0].length; i= i += 2)//Width check
        {
            if(i == 0) //Command to paint first block
                DrawingCommands.add(new DrawingCommand(Direction.DOWN+" "+0+" "+Integer.toHexString(pixels[0][0])));
            else {
                DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 2/*+""+Integer.toHexString(pixels[(pixels.length)-1][i])*/));
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[pixels.length - 1][i])));
            }
            int j = 0;
            while(j < pixels.length) //Down
            {
                int data = pixels[j][i];
                int runLength = 0;
                while(j < pixels.length && pixels[j][i] == data)
                {
                    j++;
                    runLength++;
                }
                DrawingCommands.add(new DrawingCommand(direction+" "+(runLength-1)+" "+Integer.toHexString(data)));
                //direction = (direction == Direction.LEFT) ? Direction.RIGHT : Direction.LEFT;
            }
            direction = (direction == Direction.UP) ? Direction.DOWN : Direction.UP;
        }

        return DrawingCommands;
    }

    public ArrayList<DrawingCommand> diaganalCompression()
    {
        //Field
        ArrayList<DrawingCommand> DrawingCommands = new ArrayList<DrawingCommand>();
        Direction direction = Direction.DOWN;

        //Field (In loop)
        int i = 0;
        int j = 0;
        //Field END

        while(true)//Continuous loop
        {
            //Check First
            if(i == 0 && j == 0) {
                //Check if the first pixel is the background colour
                if(pixels[i][j] == 0 && pixels[i][j+1] != 0) {
                    DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 1 + " " + Integer.toHexString(pixels[i][j+1])));
                    j++; //increment j
                } else if(pixels[i][j] != 0) { //set 0,0 then move two and set point
                    DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
                    j+=2;// Increment j by 2
                    DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 2));
                    DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
                }
            }
            //Check First END
            switch (direction)
            {
                case DOWN:
                    for(int x = j; x > 0; x--){
                        //Down 1
                        i++;
                        DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 1));
                        //Left 1
                        j--;
                        DrawingCommands.add(new DrawingCommand(Direction.LEFT + " " + 1 + " " + Integer.toHexString(pixels[i][j])));
                    }
                    direction = (direction == Direction.DOWN) ? Direction.UP : Direction.DOWN;
                    break;
                case UP:
                    for(int x = i; x > 0; x--) {
                        //Right 1
                        j++;
                        DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 1));
                        //up 1
                        i--;
                        DrawingCommands.add(new DrawingCommand(Direction.UP + " " + 1 + " " + Integer.toHexString(pixels[i][j])));
                    }
                    direction = (direction == Direction.DOWN) ? Direction.UP : Direction.DOWN;
                    break;
                default:
                    break;
            }

            if(((i+2) < pixels.length) && (j == 0)) {
                //Down 2
                i+=2;
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 2));
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
            } else if (i == (pixels.length-1) && (j+2) < pixels[0].length) {
                //Right 2
                j+=2;
                DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 2));
                DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
            } else if(((j+2) < pixels[0].length) && (i == 0)) {
                //Right 2
                j+=2;
                DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 2));
                DrawingCommands.add(new DrawingCommand(Direction.RIGHT + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
            } else if(((i == pixels[0].length-1) && (i == 0))) {
                // Down 2
                i+=2;
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 2));
                DrawingCommands.add(new DrawingCommand(Direction.DOWN + " " + 0 + " " + Integer.toHexString(pixels[i][j])));
            }
            if(i == pixels.length && j == pixels[0].length)
                break;
        }
        return DrawingCommands;
    }

    //Check to see if row1 is 0 (background colour black)
    //and if row2 (This method is rubbish too)
    public static boolean row1and2Check(int[] a, int[] b)
    {
        boolean result = true;
        //Check if the first variable in the row is 0
        result = (a[0] == 0) ? result : false;
        //Check to make sure ever value in the row is 0
        for(int i = 0; i < a.length; i++)
            result = (a[i] == 0) ? result : false;
        //Check if the second line is different from the first
        result = (a[0] != b[0]) ? result : false;
        //Check that the row has all the same values
        for(int i = 0; i < b.length; i++)
            result = (b[i] == b[0]) ? result : false;
        //return
        return result;
    }

    //Quite a nice method that checks if the commands will create an identical two dimensional array
    public static boolean ValidateAlgorithm(ArrayList<DrawingCommand> commands, int[][] pixels )
    {
        Drawing draw = new Drawing( pixels.length, pixels[0].length, 0);

        for(DrawingCommand command : commands)
            draw.addCommand(command);

        Image testEqual = null;

        try {
            testEqual = draw.draw();
            if(Arrays.deepEquals(testEqual.pixels, pixels) == true)
            {
                return true;
            }
        } catch (BadCommand badCommand) {
            return false;
        }

        return false;
    }
}
